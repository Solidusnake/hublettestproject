package com.hublet.hublettestapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.hublet.hublettestapplication.services.LocationOnBackgroundService
import com.hublet.hublettestapplication.viewmodel.MainViewModel
import org.koin.android.ext.android.get
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private lateinit var receiver: BroadcastReceiver;
    private var viewModel: MainViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_second).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)

            val i = Intent(activity, LocationOnBackgroundService::class.java)

            activity?.stopService(i)
        }

        val audioManager = activity?.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        val volumeAmountTextView = view.findViewById<TextView>(R.id.volume_amount)

        var volume_level = audioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)

        volumeAmountTextView.setText(volume_level.toString())

        view.findViewById<Button>(R.id.volume_up).setOnClickListener {

            var volumeLevel2 = viewModel?.mediaVolumeUp(requireActivity(), 1)

            volumeAmountTextView.setText(volumeLevel2.toString())
        }

        view.findViewById<Button>(R.id.volume_down).setOnClickListener {

            var volumeLevel2 = viewModel?.mediaVolumeDown(requireActivity(), 1)

            volumeAmountTextView.setText(volumeLevel2.toString())
        }

        val weatherTitleTextview = view.findViewById<TextView>(R.id.weather_title_textview)

        val humityTitleTextview = view.findViewById<TextView>(R.id.humity_title_textview)

        val timeTitleTextview = view.findViewById<TextView>(R.id.time_textview)

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                if (intent != null) {

                    val humity = intent.getLongExtra("humity", 0)

                    humityTitleTextview.setText(humity.toString() + " %")

                    val weather = intent.getStringExtra("weather")

                    weatherTitleTextview.setText(weather)

                    var volumeLevel = audioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)

                    volumeAmountTextView.setText(volumeLevel.toString())

                    val time = intent.getLongExtra("time", 0)

                    val time2 = 60000L * 60L * 3L

                    //convert seconds to milliseconds
                    val date = Date(time * 1000L);
                    // format of the date
                    val jdf = SimpleDateFormat("yyyy-MM-dd HH:mm");

                    jdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));

                    val java_date = jdf.format(date);

                    timeTitleTextview.setText(java_date)

                }

            }
        }
        val filter = IntentFilter("com.hublet.weather.intent")

        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(receiver, filter)

        viewModel = get()
    }

    override fun onDestroy() {
        super.onDestroy()

        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(receiver)
    }
}