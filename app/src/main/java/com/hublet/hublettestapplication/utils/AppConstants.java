package com.hublet.hublettestapplication.utils;

public class AppConstants {
    public static final String CHANNEL_ID = "1";
    public static final int SERVICE_LOCATION_REQUEST_CODE = 123445;

    public static final int REQ_CODE_PERMISSION = 17766;
}
