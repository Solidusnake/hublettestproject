package com.hublet.hublettestapplication

import android.location.Location
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import java.io.IOException

class WeatherRequestClient {

    companion object {
        @Throws(IOException::class)
        fun getWeatherData(location: Location): JSONObject {
            var client = OkHttpClient()

            val request: Request = Request.Builder()
                .url("https://api.openweathermap.org/data/2.5/onecall?lat=" + location.latitude.toString() + "&lon=" + location.longitude.toString() + "&exclude=current&appid=56c84fb0544867d9e72a37153ab1a9d7")
                .build()
            client.newCall(request).execute().use({ response ->

                var jsonData = JSONObject(response.body()?.string())

                return jsonData
            })
        }
    }
}
