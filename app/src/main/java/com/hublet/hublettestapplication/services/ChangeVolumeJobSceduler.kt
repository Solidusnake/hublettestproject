package com.hublet.hublettestapplication.services

import android.app.job.JobParameters
import android.app.job.JobService
import android.os.Build
import android.os.Handler
import androidx.annotation.RequiresApi


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class ChangeVolumeJobSceduler : JobService() {

    var csvHandler: Handler? = null
    override fun onStartJob(parameters: JobParameters?): Boolean {
        // runs on the main thread, so this Toast will appear
        //Toast.makeText(this, "test", Toast.LENGTH_SHORT).show()
        // perform work here, i.e. network calls asynchronously

        try {
            this.csvHandler?.removeCallbacksAndMessages(null)
        } catch (e1: Exception) {
        }
        this.csvHandler = Handler()
        this.csvHandler!!.postDelayed(object : Runnable {
            override fun run() {

            }
        }, 0)


        return true
    }

    override fun onStopJob(p0: JobParameters?): Boolean {

        return true
    }
}