package com.hublet.hublettestapplication.services

import android.Manifest
import android.R
import android.app.*
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import com.hublet.hublettestapplication.MainActivity
import com.hublet.hublettestapplication.utils.AppConstants.CHANNEL_ID
import com.hublet.hublettestapplication.viewmodel.MainViewModel
import org.koin.android.ext.android.get


class LocationOnBackgroundService : Service() {
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private lateinit var locationRequest: LocationRequest
    private var viewModel: MainViewModel? = null
    private var weatherCallback: MainViewModel.CallBackInterface? = null
    private var serviceContext = this

    companion object {

        private val MINUTE_IN_MILLISECONDS_ = 60000L

        private val FAST_UPDATE_INTERVAL: Long = (MINUTE_IN_MILLISECONDS_ * 20L)

        var isRunning = false
        private val UPDATE_INTERVAL: Long = (MINUTE_IN_MILLISECONDS_ * 30L)
    }

    override fun onCreate() {
        super.onCreate()
        initData()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val input = intent.getStringExtra("inputExtra")
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )


        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Voice level Service")
            .setContentText(input)
            .setSmallIcon(R.drawable.btn_default)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(pendingIntent)
            .build()

        startForeground(1, notification)

        startLocationUpdates()

        isRunning = true

        return START_STICKY
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Voice level Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mFusedLocationClient!!.removeLocationUpdates(locationCallback)

        try {
            handler.removeCallbacksAndMessages(null)
        } catch (e1: Exception) {

        }

        isRunning = false
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient!!.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )

        mFusedLocationClient!!.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                weatherCallback?.let { viewModel!!.loadWeather(it, location) }
            }
        }
    }

    private fun initData() {

        locationRequest = LocationRequest.create()

        locationRequest.setInterval(UPDATE_INTERVAL)
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setFastestInterval(FAST_UPDATE_INTERVAL)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext)

        viewModel = get()

        weatherCallback = object : MainViewModel.CallBackInterface {
            override fun callback() {

                startGoingThrougWeatherData()
            }
        }
    }

    val handler = Handler()
    var firstTime = true

    private fun startGoingThrougWeatherData() {

        firstTime = false

        try {
            handler.removeCallbacksAndMessages(null)
        } catch (e1: Exception) {

        }

        handler.postDelayed(object : Runnable {
            override fun run() {

                try {
                    handler.removeCallbacksAndMessages(null)
                } catch (e1: Exception) {

                }

                viewModel?.goThrouthData(serviceContext)

                handler.postDelayed(this, MINUTE_IN_MILLISECONDS_ * 2)
            }
        }, MINUTE_IN_MILLISECONDS_ / 20)
    }

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            val currentLocation = locationResult.lastLocation
            if (currentLocation != null) {
                weatherCallback?.let { viewModel!!.loadWeather(it, currentLocation) }
            }
        }
    }
}