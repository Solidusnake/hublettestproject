package com.hublet.hublettestapplication.application

import com.hublet.hublettestapplication.viewmodel.MainViewModel
import com.hublet.hublettestapplication.viewmodel.WeatherEntity
import org.koin.dsl.module


// Given some classes
class Controller(val service: BusinessService)
class BusinessService()
class MainViewModel()
class Observer<MutableList>()

// just declare it
val myModule = module {
    single { Controller(get()) }
    single { BusinessService() }
    single { MainViewModel() }

    single { Observer<MutableList<WeatherEntity>>() }
}
