package com.hublet.hublettestapplication

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.hublet.hublettestapplication.services.LocationOnBackgroundService


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_first).setOnClickListener {
            if (locationEnabled()) {

                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)

                startLocationUpdatesService()

                //findNavController().popBackStack(R.id.FirstFragment, true);
            } else {
                showDialog()
            }
        }

        if (LocationOnBackgroundService.isRunning) {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)

        }
    }

    private fun startLocationUpdatesService() {

        val i = Intent(activity, LocationOnBackgroundService::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity?.startForegroundService(i)
        } else {
            activity?.startService(i)
        }
    }

    private fun locationEnabled(): Boolean {
        val lm = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        var gps_enabled = false
        var network_enabled = false
        try {
            gps_enabled = lm!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            network_enabled = lm!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (!gps_enabled && !network_enabled) {

            return false
        }

        return true
    }

    private fun showDialog() {
        activity?.let {
            AlertDialog.Builder(it)
                .setTitle("Enable GPS")
                .setMessage("GPS not enabled. Enable GPS from settings")
                .setPositiveButton("Settings",
                    DialogInterface.OnClickListener { paramDialogInterface, paramInt ->
                        startActivity(
                            Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        )
                    })
                .setNegativeButton("Cancel", null)
                .show()
        }
    }
}