package com.hublet.hublettestapplication

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.hublet.hublettestapplication.services.LocationOnBackgroundService.Companion.isRunning
import com.hublet.hublettestapplication.utils.AppConstants.REQ_CODE_PERMISSION
import com.hublet.hublettestapplication.viewmodel.MainViewModel
import com.hublet.hublettestapplication.viewmodel.WeatherEntity
import org.koin.android.ext.android.get


class MainActivity : AppCompatActivity() {


    lateinit var viewModel: MainViewModel
    private var changeObserver = Observer<MutableList<WeatherEntity>> {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        if (checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) == false) {
            val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

            ActivityCompat.requestPermissions(
                this,
                permissions,
                REQ_CODE_PERMISSION
            )
        }/*else
        if(checkPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION)==false){
            val permissions = arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)

            ActivityCompat.requestPermissions(
                this,
                permissions,
                REQ_CODE_PERMISSION
            )
        }*/


        viewModel = get()


        changeObserver = Observer<MutableList<WeatherEntity>> { weatherEntitysArray ->
            val size = weatherEntitysArray.size


        }

        viewModel.getWeather().observe(this, changeObserver)

        if (isRunning) {
            //findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
    }


    fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQ_CODE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) == false) {
                val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

                ActivityCompat.requestPermissions(
                    this,
                    permissions,
                    REQ_CODE_PERMISSION
                )
            }/*else
            if(checkPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION)==false){

                val permissions = arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)

                ActivityCompat.requestPermissions(
                    this,
                    permissions,
                    REQ_CODE_PERMISSION
                )
            }*/

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun onBackPressed() {
        super.onBackPressed();
        finish()

    }
}