package com.hublet.hublettestapplication.viewmodel

class WeatherEntity {

    var precipitation = 0L

    var dt = 0L

    var sunrice = 0L

    var sunset = 0L

    var temp = 0L

    var feelslike = 0L


    var pressure = 0L

    var humidity = 0L

    var uvi = 0L


    var clouds = 0L

    var visibility = 0L

    var wind_speed = 0L

    var winddeg = 0L


    var main = ""

    var description = ""
}