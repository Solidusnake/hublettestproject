package com.hublet.hublettestapplication.viewmodel

import android.content.Context
import android.content.Intent
import android.location.Location
import android.media.AudioManager
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.hublet.hublettestapplication.WeatherRequestClient.Companion.getWeatherData
import java.util.*


class MainViewModel : ViewModel() {
    // TODO: Implement the ViewModel
    private val weatherEntitys = MutableLiveData<MutableList<WeatherEntity>>()
    private val weatherEntityList = mutableListOf<WeatherEntity>()


    fun getWeather(): MutableLiveData<MutableList<WeatherEntity>> {

        return weatherEntitys
    }

    fun getWeatherList(): MutableList<WeatherEntity>? {

        return weatherEntityList
    }

    fun loadWeather(callBackInterface: CallBackInterface, location: Location) {

        val thread = Thread {
            val weatherData = getWeatherData(location)

            weatherEntityList.clear()

            val hourly = weatherData.getJSONArray("hourly")

            for (x in 0..hourly.length() - 1) {
                val firstHourly = hourly.getJSONObject(x)

                val weatherEntity = WeatherEntity()

                weatherEntity.dt = firstHourly.getLong("dt")

                weatherEntity.humidity = firstHourly.getLong("humidity")

                val weatherDescription = firstHourly.getJSONArray("weather")

                val weatherRow = weatherDescription.getJSONObject(0)

                weatherEntity.description = weatherRow.getString("description")

                weatherEntityList.add(weatherEntity)
            }

            callBackInterface.callback()

            Handler(Looper.getMainLooper()).post {

                weatherEntitys.postValue(weatherEntityList)
            }
        }
        thread.start()
    }

    private var isRaining = false
    private var previuslyRaining = false
    private var humidity = -1L

    fun checkHumityChange(mutableList2: MutableList<WeatherEntity>): WeatherEntity {

        var sortedList = mutableList2

        val sortedDates = sortedList.sortedBy { it.dt }

        isRaining = false

        val currentTime = (Calendar.getInstance().timeInMillis / 1000)

        for (x in 0..sortedDates!!.size - 1) {

            if (sortedDates.get(x).dt >= currentTime) {

                return sortedDates.get(x)
            }
        }

        return WeatherEntity()
    }

    fun addjustVolume(context: Context, humidityChange: Long) {

        if (humidityChange > 0L) {

            if (humidityChange != null) {
                mediaVolumeUpAmount(context, humidityChange)
            }

        } else if (humidityChange < 0L) {

            previuslyRaining = false

            if (humidityChange != null) {
                mediaVolumeDownAmount(context, humidityChange)
            }

        }
    }

    fun mediaVolumeDownAmount(context: Context, humityChange: Long) {

        val amount = humityChange / 3

        mediaVolumeDown(context, amount.toInt())

    }

    fun mediaVolumeDown(context: Context, amount: Int): Int {
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        /*audioManager.adjustVolume(
            AudioManager.ADJUST_LOWER,
            AudioManager.FLAG_PLAY_SOUND
        )*/

        var volumeLevel = audioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)

        volumeLevel = volumeLevel - amount


        if (volumeLevel > 0) {
            audioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                volumeLevel,
                AudioManager.FLAG_PLAY_SOUND
            );
        }

        return volumeLevel
    }

    fun mediaVolumeUpAmount(context: Context, humityChange: Long) {

        val amount = humityChange / 3

        mediaVolumeUp(context, amount.toInt())

    }


    fun mediaVolumeUp(context: Context, amount: Int): Int {
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        /*audioManager.adjustVolume(
            AudioManager.ADJUST_RAISE,
            AudioManager.FLAG_PLAY_SOUND,
        )*/

        var volumeLevel = audioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)

        volumeLevel = volumeLevel + amount

        audioManager.setStreamVolume(
            AudioManager.STREAM_MUSIC,
            volumeLevel,
            AudioManager.FLAG_PLAY_SOUND
        );

        return volumeLevel
    }

    fun goThrouthData(serviceContext: Context) {

        val weatherData = getWeatherList()

        val weatherEntity = weatherData?.let { checkHumityChange(it) }

        val intent = Intent("com.hublet.weather.intent")

        intent.putExtra("weather", weatherEntity?.description)

        intent.putExtra("humity", weatherEntity?.humidity);

        intent.putExtra("time", weatherEntity?.dt);

        LocalBroadcastManager.getInstance(serviceContext).sendBroadcast(intent);

        if (humidity == -1L) {
            if (weatherEntity != null) {
                humidity = weatherEntity.humidity
            }
        }

        val humidyChange = weatherEntity?.humidity?.minus(humidity)

        if (humidyChange != null) {
            addjustVolume(serviceContext, humidyChange)
        }

        if (weatherEntity != null) {
            humidity = weatherEntity.humidity
        }
    }

    interface CallBackInterface {
        fun callback()
    }

}
